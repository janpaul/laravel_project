<?php

use App\User;

function super_admin(){
   $email = 'superadmin@gmail.com';
   return User::where('email',$email)->first();
}

function staff_user(){
    $email = 'staff@gmail.com';
    return User::where('email',$email)->first();
}

function readonly_user(){
    $email = 'readonly@gmail.com';
    return User::where('email',$email)->first();
}  