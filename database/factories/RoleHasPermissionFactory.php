<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\RoleHasPermission;
use Faker\Generator as Faker;

$factory->define(RoleHasPermission::class, function (Faker $faker) {
    return [
       'role_id' => 2,
       'permission_id' => 3
    ];
});
