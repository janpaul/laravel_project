<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{

    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // user permissions
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'update users']);
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'delete users']);


        // role permissions
        $role_pemission = Permission::insert([
            ['name' => 'view roles', 'guard_name' => 'web'],
            ['name' => 'create roles', 'guard_name' => 'web'],
            ['name' => 'update roles', 'guard_name' => 'web'],
            ['name' => 'delete roles', 'guard_name' => 'web'],
        ]);

        $permission = Permission::insert([
            ['name' => 'view permissions', 'guard_name' => 'web'],
            ['name' => 'create permissions', 'guard_name' => 'web'],
            ['name' => 'update permissions', 'guard_name' => 'web'],
            ['name' => 'delete permissions', 'guard_name' => 'web'],
        ]);


        // assign or removed permissions to roles
        $assign_remove_permission_to_role = Permission::insert([
            ['name' => 'assign permission to role', 'guard_name' => 'web'],
            ['name' => 'removed permission to role', 'guard_name' => 'web']
        ]);

        $super_admin = Role::create([
            'name' => 'super_admin'
        ]);

        $user = Role::create([
            'name' => 'user'
        ]);

        $staff = Role::create([
            'name' => 'staff'
        ]);

        $staff->givePermissionTo('view users');
        // $staff->givePermissionTo('assign permission to role');
    }
}
