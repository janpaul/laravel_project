<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function store($role, $data)
    {
        $user = User::create($data);
        $user->assignRole($role);
    }

    public function run()
    {
        $super_admin = User::create([
            'email' => 'superadmin@gmail.com',
            'address' => 'south carolina 25 street',
            'contact_no' => '212399421',
            'name' => 'linsy guino',
            'password' => '12345',
        ]);

        $super_admin->assignRole("super_admin");

        $staff = User::create([
            'email' => 'staff@gmail.com',
            'address' => 'norht bound huts street',
            'contact_no' => '2326689999',
            'name' => 'geron adam',
            'password' => '12345'
        ]);

        $staff->assignRole("staff");


        $user = User::create([
            'email' => 'user@gmail.com',
            'address' => 'hillside street',
            'contact_no' => '9889625',
            'name' => 'thea bhrend',
            'password' => '12345'
        ]);
        $user->assignRole("user");
    }
}
