<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function login_test()
    {
        $email = 'test@gmail.com';
        $password = '12345';
        $user = factory('App\User')->create(
            [
                'email' => $email,
                'password' => $password
            ]
        );

        $response = $this->post('/api/login', [
            'email' => $email,
            'password' => $password
        ]);
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'user' =>[
                'id',
                'email'
            ]
        ]);
        $response->dump();
        $response->assertStatus(200);
    }
}
