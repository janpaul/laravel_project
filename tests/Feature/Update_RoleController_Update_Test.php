<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class Update_RoleController_Update_Test extends TestCase
{
   use RefreshDatabase,WithFaker;

    public function setUp():void{
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function update_role_test()
    {
        $this->actingAs(super_admin(),'api');
        $role = Role::first();
        $data = [
            'name' => 'admin'
        ];
        $response = $this->patch('/api/roles/'.$role->id,$data);
        $response->dump();
        $this->assertDatabaseHas('roles',$data);
        $response->assertStatus(200);
    }
}
