<?php

namespace Tests\Feature;

use App\RoleHasPermission;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class RemovedPermissionToRole_Test extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function removed_permission_to_role()
    {

        $this->actingAs(super_admin(), 'api');
        $role = Role::where('name', 'staff')->first();
        $permission = Permission::find(2);

        $data =  [
            'role_id' => $role->id,
            'permission_id' => $permission->id
        ];
        $response = $this->post('/api/assign-permission', $data);


        $response_remove = $this->post('/api/remove-permission', $data);
        $response_remove->dump();
        $this->assertDatabaseMissing('role_has_permissions', $data);
    }
}
