<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class AssignPermissionToRole_Test extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function assign_permissin_to_role()
    {
        $this->actingAs(super_admin(), 'api');
        $role = Role::where('name', 'staff')->first();
        $permission = Permission::find(2);

        $response = $this->post('/api/assign-permission', [
            'role_id' => $role->id,
            'permission_id' => $permission->id
        ]);
        $this->assertDatabaseHas('role_has_permissions', [
            'role_id' => $role->id,
            'permission_id' => $permission->id
        ]);
        $response->assertStatus(200);
    }

    // test user permissions
    /** @test */
    public function a_user_with_no_permission_to_assign_permission_to_role_will_be_denied()
    {
        $this->expectException("Spatie\Permission\Exceptions\UnauthorizedException");
        $this->actingAs(staff_user(), 'api');
        $role = Role::where('name', 'staff')->first();
        $permission = Permission::find(2);

        $response = $this->post('/api/assign-permission', [
            'role_id' => $role->id,
            'permission_id' => $permission->id
        ]);
    }
}
