<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class Fetch_Role_RoleController_Index_Test extends TestCase
{
    use RefreshDatabase;
    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }
    /** @test */
    public function fetch_roles_test()
    {
        $this->actingAs(super_admin(), 'api');

        $response = $this->get('/api/roles');
        $response->dump();
        $response->assertStatus(200);

        $response->assertJsonStructure([
            'roles' => [
                [
                    'name',
                    'permissions'
                ]
            ]
        ]);
    }

    /** @test */
    public function fetch_role_by_id_test()
    {
        $this->actingAs(super_admin(), 'api');
        $role = Role::first();
        $response = $this->get('/api/roles/' . $role->id);
        $response->dump();
        $response->assertStatus(200);

        $response->assertJsonStructure([
            'roles' => [
                'name'
            ]
        ]);
    }
}
