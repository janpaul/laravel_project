<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class Create_Role_RoleController_Store_Test extends TestCase
{

    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function create_role_test()
    {
        $data = [
            'name' => 'role admin',
            'guard_name' => 'web'
        ];

        $this->actingAs(super_admin(), 'api');

        $response = $this->post('/api/roles', $data);

        $response->dump();

        $this->assertDatabaseHas('roles', $data);
        $response->assertStatus(200);

    }

    // test permissions on creating role
    /** @test */
    public function user_with_no_permission_creating_role_will_be_denied(){
        $this->expectException("Spatie\Permission\Exceptions\UnauthorizedException");
        $data = [
            'name' => 'new role my new role',
            'guard_name' => 'web'
        ];
        $this->actingAs(staff_user(), 'api');
        // dd(auth()->user()->getAllPermissions());
        $response = $this->post('/api/roles', $data);
        $response->assertStatus(200);
    }
}
