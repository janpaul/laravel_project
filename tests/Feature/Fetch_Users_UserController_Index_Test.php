<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Fetch_Users_UserController_Index_Test extends TestCase
{

    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function fetch_users_test()
    {
        $this->actingAs(super_admin(), 'api');

        $response = $this->get('/api/users');
        $response->dump();
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'users' => [
                [
                    'id',
                    'email',
                    'address',
                ],
            ]
        ]);
    }

    /** @test */
    public function fetch_user_by_id()
    {
        $this->actingAs(super_admin(), 'api');

        $response = $this->get('/api/users/2');
        $response->dump();
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'users' => [
                'id',
                'email',
                'address'
            ]
        ]);
    }
}
