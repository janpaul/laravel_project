<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class Create_User_UserController_Store_Test extends TestCase
{

    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function create_user_test()
    {
        $this->actingAs(super_admin(), 'api');

        $data = [
            'role_id' => '3',
            'name' => $this->faker()->name(),
            'address' => $this->faker()->address,
            'email' => $this->faker()->email,
            'contact_no' => $this->faker()->phoneNumber,
            'password' => $this->faker()->phoneNumber,
        ];
        $response = $this->post('/api/users', $data);
        $response->dump();
        $this->assertDatabaseHas(
            'users',
            collect($data)->except(['password','role_id'])->toArray()
        );

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'message'
        ]);
    }

    // test the validation for email missing
    /** @test */
    public function test_email_validation_testing_test()
    {
        $this->actingAs(super_admin(), 'api');
        $this->expectException('Illuminate\Validation\ValidationException');
        $data = [
            'name' => $this->faker()->name(),
            'address' => $this->faker()->address,
            'email' => '',
            'contact_no' => $this->faker()->phoneNumber,
            'password' => $this->faker()->phoneNumber,
        ];
        $response = $this->post('/api/users', $data);
    }

    // testing permission for creating user
    /** @test */
    public function only_user_with_permission_to_create_user_can_successfully_create_user()
    {
        $this->expectException('Spatie\Permission\Exceptions\UnauthorizedException');
        $this->actingAs(staff_user(), 'api');
        $data = [
            'name' => $this->faker()->name(),
            'address' => $this->faker()->address,
            'email' => $this->faker()->email,
            'contact_no' => $this->faker()->phoneNumber,
            'password' => $this->faker()->phoneNumber,
        ];
        $response = $this->post('/api/users', $data);
    }
}
