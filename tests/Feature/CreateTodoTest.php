<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateTodoTest extends TestCase
{

     use RefreshDatabase;
    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function create_todo_test()
    {
        $data = [
            "todo" => 'test todo'
        ];
        $this->actingAs(super_admin(), 'api');
        $response = $this->post('/api/todos',$data);

        $this->assertDatabaseHas('todos',$data);
        $response->assertStatus(200);
    }
}
