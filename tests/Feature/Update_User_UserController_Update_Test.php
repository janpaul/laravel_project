<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Update_User_UserController_Update_Test extends TestCase
{

    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function update_user()
    {
        $this->actingAs(super_admin(), 'api');
        $user = factory('App\User')->create();
        $data = [
            'email' => 'updatedemail@gmail.com',
            'name' => ''
        ];
        $response = $this->patch('/api/users/' . $user->id, $data);
        $this->assertDatabaseHas('users', [
            'email' => $data['email']
        ]);
        $response->assertStatus(200);
    }
}
