<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Create_MobileAppUser_Test extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function create_user_test()
    {
 
        $data = [
            'name' => $this->faker()->name(),
            'email' => $this->faker()->email,
            'password' => $this->faker()->password()
        ];
        $response = $this->post('/api/user-mobile', $data);
        $response->dump();
        $this->assertDatabaseHas(
            'users',
            collect($data)->except(['password'])->toArray()
        );

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'message'
        ]);
    }

    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
