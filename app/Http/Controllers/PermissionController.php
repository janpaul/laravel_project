<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:view permissions'])->only(['index']);
    }
    public function index()
    {
        return response()->json([
            'permissions' => Permission::get()
        ]);
    }
}
