<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssignPermisionRequest;
use App\Services\AssignPermissionToRoleService;
use Illuminate\Http\Request;

class AssignPermissionToRoleController extends Controller
{
    public function __construct()
    {
        $this->assign_permission_service = new AssignPermissionToRoleService;
        $this->middleware(['permission:assign permission to role'])->only(['store']);
        $this->middleware(['permission:removed permission to role'])->only(['delete']);
    }
    public function index()
    {
    }

    public function store(AssignPermisionRequest $request)
    {
        $this->assign_permission_service->assign_permission($request->role_id, $request->permission_id);

        return response()->json([
            'message' => 'permission was successfulyy assigend to role'
        ]);
    }

    public function remove_permission(AssignPermisionRequest $request)
    {
        $this->assign_permission_service->removed_permission($request->role_id, $request->permission_id);
        return response()->json([
            'message' => 'permission was removed to the role'
        ]);
    }
}
