<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Services\UserService;
use Illuminate\Http\Request;

class MobileAppUserController extends Controller
{
    public function __construct()
    {
        $this->user_service = new UserService;
    }

    public function store(Request $request){

        $this->user_service->create_user_toggle($request->all());
     
        return response()->json([
            'message' => 'successfully registered'
        ]);
    }
}
