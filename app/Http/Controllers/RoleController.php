<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:create roles'])->only(['store']);
        $this->middleware(['permission:view roles'])->only(['show', 'index']);
        $this->middleware(['permission:update roles'])->only(['update']);
        $this->middleware(['permission:delete roles'])->only(['destroy']);
    }


    public function index()
    {
        return response()->json([
            'roles' => Role::with('permissions')->where('name', '!=', 'super_admin')->get()
        ]);
    }

    public function store(RoleRequest $request)
    {
        Role::create($request->validated());

        return response()->json([
            'message' => 'role was successfully created'
        ]);
    }

    public function show(Role $role)
    {
        return response()->json([
            'roles' => $role
        ]);
    }


    public function update(RoleRequest $request, Role $role)
    {
        $role->update(array_filter($request->validated()));

        return response()->json([
            'message' => 'role updated'
        ]);
    }


    public function destroy($id)
    {
        //
    }
}
