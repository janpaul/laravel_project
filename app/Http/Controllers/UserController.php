<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Resources\User as ResourcesUser;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:create users'])->only(['store']);
        $this->middleware(['permission:view users'])->only(['show', 'index']);
        $this->middleware(['permission:update users'])->only(['update']);
        $this->middleware(['permission:delete users'])->only(['destroy']);
        $this->user_service = new UserService;
    }

    public function index()
    {
        return response()->json([
            'users' => ResourcesUser::collection(User::get())
        ]);
    }

    public function store(UserRequest $request)
    {
        $this->user_service->create_user_toggle($request->validated());
     
        return response()->json([
            'message' => 'success'
        ]);
    }

    public function show(User $user)
    {
        return response()->json([
            'users' => new ResourcesUser($user)
        ]);
    }

    public function update(UserRequest $request, User $user)
    {
        $user->update(array_filter($request->validated()));
        return response()->json([
            'message' => 'user updated'
        ]);
    }
}
