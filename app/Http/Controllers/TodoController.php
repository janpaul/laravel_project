<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoRequest;
use App\Services\TodoService;
use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{

    public function __construct()
    {
        $this->todo_service = new TodoService();
    }

    public function index()
    {
        return response()->json([
            'todos' => auth()->user()->todos
        ]);
    }

    public function store(TodoRequest $request)
    {
        auth()->user()->todos()->create($request->validated());
        return response()->json([
            'message' => 'todo successfully created'
        ]);
    }


    public function show(Todo $todo)
    {
    }


    public function update(Request $request, Todo $todo)
    {
        $this->todo_service->todo_toggle($todo->id);
            
        return response()->json([
            'todos' => 'mark as done'
        ]);
    }

    public function destroy(Todo $todo)
    {
    }
}
