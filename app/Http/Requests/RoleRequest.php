<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        if ($this->isMethod('post')) {
            return [
                'name' => 'required|max:50|min:3',
                'guard_name' => 'required',
            ];
        }

        if ($this->isMethod('put') || $this->method('patch')) {
            return [
                'name' => 'sometimes|max:50|min:3',
                'guard_name' => 'sometimes|min:2',
            ];
        }
    }
}
