<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignPermisionRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        if ($this->isMethod('post')) {
            return [
                'role_id' => 'required|exists:Spatie\Permission\Models\Role,id',
                'permission_id' => 'required|exists:Spatie\Permission\Models\Permission,id',
            ];
        }
 
    }
}
