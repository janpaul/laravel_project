<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        if ($this->isMethod("post")) {
            return [
                'name' => 'required|max:50',
                'email' => 'required|email|unique:users',
                'address' => 'required|max:200',
                'contact_no' => 'required|min:5|max:30',
                'password' => 'required|min:5:max:50',
                'role_id' => 'sometimes'
            ];
        }

        if ($this->isMethod("put") || $this->isMethod("patch")) {
            return [
                'name' => 'sometimes|max:50',
                'email' => 'sometimes|email',
                'address' => 'sometimes|max:200',
                'contact_no' => 'sometimes|min:5|max:30',
                'password' => 'sometimes|min:5:max:50'
            ];
        }
    
    }
}
