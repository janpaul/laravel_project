<?php

namespace App\Services;

use App\User;
use Spatie\Permission\Models\Role;

class UserService
{

    public function __construct()
    {
        $this->user = new User();
        $this->role = new Role();
    }

    public function find_role($role_id)
    {
        return $this->role->find($role_id);
    }

    public function create_system_user($data)
    {
        $user = $this->user->create($data);
        $user->assignRole($this->find_role($data['role_id']));
    }


    public function create_mobile_user($data)
    {
        $user = $this->user->create($data);
        $user->assignRole("user");
    }

    public function create_user_toggle($data)
    {
        $flip = array_flip($data);
        if (in_array('role_id', $flip)) {
            $this->create_system_user($data);
        } else {
            $this->create_mobile_user($data);
        }
    }
}
