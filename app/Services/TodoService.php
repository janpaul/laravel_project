<?php

namespace App\Services;

use App\Todo;

class TodoService{

    public function __construct()
    {
        $this->todo = new Todo();
    }


    public function mark_complete($todo){
        return $todo->update([
            'is_done' => "true"
        ]);
    }

    public function mark_not_complete($todo){
        return $todo->update([
            'is_done' => "false"
        ]);
    }
    public function todo_toggle($todo_id){
        $todo = $this->todo->find($todo_id);
        if($todo->is_done == "true"){
            $this->mark_not_complete($todo);
        }else{
            $this->mark_complete($todo);
        }
    }
}