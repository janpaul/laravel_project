<?php

namespace App\Services;

use App\Exceptions\AssignPermisionException;
use App\RoleHasPermission;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AssignPermissionToRoleService
{

    public function __construct()
    {
        $this->role = new Role();
        $this->permission = new Permission();
        $this->role_has_permission = new RoleHasPermission();
    }

    public function find_role($id)
    {
        return $this->role->findOrFail($id);
    }

    public function find_permission($id)
    {
        return $this->permission->findOrFail($id);
    }

    public function check_if_permission_assigned_already($role_id, $permission_id)
    {
        if ($this->role_has_permission->where('role_id', $role_id)
            ->where('permission_id', $permission_id)->exists()
        ) {
            throw new AssignPermisionException("this permissions is already assigned to the role");
        }
    }

    public function assign_permission($role_id, $permission_id)
    {
        $this->check_if_permission_assigned_already($role_id, $permission_id);
        $role = $this->find_role($role_id);
        $role->givePermissionTo($this->find_permission($permission_id)->name);
    }

    public function removed_permission($role_id, $permission_id)
    {
        $role = $this->find_role($role_id);
        $role->revokePermissionTo($this->find_permission($permission_id));
    }
}
