<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = ['user_id','todo','is_done'];
    
    protected $cast = [
        'created_at' => 'datetime:Y-m-d H:i'
    ];
}
