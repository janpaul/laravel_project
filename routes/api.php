<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/login', 'LoginController@login');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'LoginController@logout');
    Route::resource('/users', 'UserController');
    Route::resource('/roles', 'RoleController');
    Route::resource('/permissions', 'PermissionController');
    Route::resource('/assign-permission', 'AssignPermissionToRoleController');
    Route::post('/remove-permission', 'AssignPermissionToRoleController@remove_permission');
    Route::resource('/todos', 'TodoController');
});

Route::post('/user-mobile', 'MobileAppUserController@store');
